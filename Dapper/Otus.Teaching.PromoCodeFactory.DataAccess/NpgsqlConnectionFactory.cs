using Npgsql;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public sealed class NpgsqlConnectionFactory
{
    private readonly string _connectionString;

    public NpgsqlConnectionFactory(string connectionString) => _connectionString = connectionString;

    public NpgsqlConnection CreateSqlConnection() => new (_connectionString);
}