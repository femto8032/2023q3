using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Models.Customer;
using Otus.Teaching.PromoCodeFactory.Core.Models.Preference;
using Otus.Teaching.PromoCodeFactory.Core.Models.PromoCode;
using Otus.Teaching.PromoCodeFactory.DataAccess.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public sealed class CustomerRepository : ICustomerRepository
{
    private readonly NpgsqlConnectionFactory _connectionFactory;
    private readonly ILogger<CustomerRepository> _logger;

    public CustomerRepository(ILogger<CustomerRepository> logger, NpgsqlConnectionFactory connectionFactory)
    {
        _logger = logger;
        _connectionFactory = connectionFactory;
    }

    public async Task<IReadOnlyList<CustomerShortResponse>> GetAll(CancellationToken cancellationToken)
    {
        await using var sqlConnection = _connectionFactory.CreateSqlConnection();

        var result = await sqlConnection
            .QueryAsync<CustomerShortResponse>(
                new CommandDefinition(
                    """
                    SELECT c."Id", c."FirstName", c."LastName", c."Email"
                    FROM public."Customers" c
                    """,
                    cancellationToken: cancellationToken));
        
        return result.ToList();
    }

    public async Task<CustomerResponse> FindById(Guid id, CancellationToken cancellationToken)
    {
        await using var sqlConnection = _connectionFactory.CreateSqlConnection();

        var lookup = new Dictionary<Guid, CustomerResponse>();
        await sqlConnection
            .QueryAsync<CustomerResponse, PromoCodeShortResponse, PreferenceShortResponse, CustomerResponse>(
                new CommandDefinition(
                    """
                    SELECT c."Id", c."FirstName", c."LastName", c."Email", pc."Id", pc."Code", pc."ServiceInfo", pc."BeginDate", pc."EndDate", pc."PartnerName", p."Name"
                    FROM public."Customers" c
                    INNER JOIN public."PromoCodes" pc ON pc."CustomerId" = c."Id"
                    INNER JOIN "CustomerPreferences" cp ON cp."CustomerId" = c."Id"
                    INNER JOIN "Preferences" p ON p."Id"  = cp."PreferenceId"
                    WHERE c."Id" = @id
                    """,
                    new { id },
                    cancellationToken: cancellationToken),
                (customer, code, preference) =>
                {
                    if (!lookup.TryGetValue(customer.Id, out var course)) 
                        lookup.Add(customer.Id, course = customer);
                    
                    course.PromoCodes ??= new List<PromoCodeShortResponse>();
                    course.PromoCodes.Add(code);
                    
                    course.Preferences ??= new List<PreferenceShortResponse>();
                    course.Preferences.Add(preference);
                    
                    return course;
                },
                $"{nameof(Customer.Id)}, {nameof(Preference.Name)}");
        
        return lookup[id];
    }
    
    public async Task<int> DeleteAsync(Guid id, CancellationToken cancellationToken)
    {
        await using var sqlConnection = _connectionFactory.CreateSqlConnection();
        return await sqlConnection.ExecuteAsync(
            new CommandDefinition(
            """DELETE FROM public."Customers" WHERE "Customers"."Id" = @id """, 
            new { id }, 
            cancellationToken: cancellationToken));
    }
    
    public async Task<bool> AnyAsync(Guid id, CancellationToken cancellationToken)
    {
        await using var sqlConnection = _connectionFactory.CreateSqlConnection();
        return await sqlConnection.ExecuteScalarAsync<bool>(
            new CommandDefinition(
                """SELECT EXISTS (SELECT 1 FROM public."Customers" WHERE "Customers"."Id" = @id)""", 
                new { id }, 
                cancellationToken: cancellationToken));
    }

    public async Task<Customer> CreateCustomerWithPreference(Customer entity, IReadOnlyList<CustomerPreference> customerPreferences, CancellationToken cancellationToken)
    {
        await using var sqlConnection = _connectionFactory.CreateSqlConnection();
        await sqlConnection.OpenAsync(cancellationToken);
        await using var transaction = await sqlConnection.BeginTransactionAsync(IsolationLevel.ReadUncommitted, cancellationToken);

        try
        {
            int rowsAffected = await sqlConnection.ExecuteAsync(
                new CommandDefinition(
                    """
                    INSERT INTO public."Customers"
                    ("Id", "FirstName", "LastName", "Email")
                    VALUES(@Id, @FirstName, @LastName, @Email)
                    """, 
                    new { Id = entity.Id, FirstName = entity.FirstName, LastName = entity.LastName, Email = entity.Email },
                    transaction,
                    cancellationToken: cancellationToken));
            
            await using var writer = await sqlConnection?.BeginBinaryImportAsync(
                """ COPY public."CustomerPreferences" ("CustomerId", "PreferenceId") FROM STDIN (FORMAT BINARY) """,
                cancellationToken)!;
            
            foreach (var preference in customerPreferences)
            {
                await writer.StartRowAsync(cancellationToken);
                await writer.WriteAsync(preference.CustomerId, cancellationToken);
                await writer.WriteAsync(preference.PreferenceId, cancellationToken);
            }
            
            await writer.CompleteAsync(cancellationToken);
            await writer.CloseAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return rowsAffected > 0
                ? entity 
                : throw new DatabaseException();
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Error, e.Message);

            await transaction.RollbackAsync(cancellationToken);
            throw new DatabaseException("Cannot create customer.");
        }
        finally
        {
            await sqlConnection?.CloseAsync()!;
        }
    }

    public async Task<Customer> UpdateCustomerWithPreference(Customer entity, IEnumerable<CustomerPreference> customerPreferences, CancellationToken cancellationToken)
    {
        await using var sqlConnection = _connectionFactory.CreateSqlConnection();
        await sqlConnection.OpenAsync(cancellationToken);
        await using var transaction = await sqlConnection.BeginTransactionAsync(IsolationLevel.ReadUncommitted, cancellationToken);
        
        try
        {
            int rowsAffected = await sqlConnection.ExecuteAsync(
                new CommandDefinition(
                    """
                    UPDATE public."Customers"
                    SET "FirstName" = @FirstName, "LastName" = @LastName, "Email" = @Email
                    WHERE "Id" = @Id
                    """,
                    new { Id = entity.Id, FirstName = entity.FirstName, LastName = entity.LastName, Email = entity.Email },
                    transaction,
                    cancellationToken: cancellationToken));
        
            await sqlConnection.ExecuteAsync(
                new CommandDefinition(
                    """DELETE FROM public."CustomerPreferences" WHERE "CustomerPreferences"."CustomerId" = @Id """, 
                    new { Id = entity.Id },
                    transaction,
                    cancellationToken: cancellationToken));
            
            await using var writer = await sqlConnection?.BeginBinaryImportAsync(
                """ COPY public."CustomerPreferences" ("CustomerId", "PreferenceId") FROM STDIN (FORMAT BINARY) """,
                cancellationToken)!;
            
            foreach (var preference in customerPreferences)
            {
                await writer.StartRowAsync(cancellationToken);
                await writer.WriteAsync(preference.CustomerId, cancellationToken);
                await writer.WriteAsync(preference.PreferenceId, cancellationToken);
            }
            
            await writer.CompleteAsync(cancellationToken);
            await writer.CloseAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return rowsAffected > 0
                ? entity 
                : throw new DatabaseException();
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Error, e.Message);

            await transaction.RollbackAsync(cancellationToken);
            throw new DatabaseException("Cannot update customer.");
        }
        finally
        {
            await sqlConnection?.CloseAsync()!;
        }
    }
}