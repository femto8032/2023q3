using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Options;

public sealed record DatabaseOptions
{
    [Required]
    public string ConnectionString { get; init; }
    
    [Required]
    [Range(0, 1000)]
    public int CommandTimeout { get; init; }
}