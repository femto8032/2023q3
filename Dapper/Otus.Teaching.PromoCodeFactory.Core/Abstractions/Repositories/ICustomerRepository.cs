using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Models.Customer;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface ICustomerRepository
{
    Task<IReadOnlyList<CustomerShortResponse>> GetAll(CancellationToken cancellationToken);
        
    Task<CustomerResponse> FindById(Guid id, CancellationToken cancellationToken);
        
    Task<int> DeleteAsync(Guid id, CancellationToken cancellationToken);

    Task<bool> AnyAsync(Guid id, CancellationToken cancellationToken);
    
    Task<Customer> CreateCustomerWithPreference(Customer entity, IReadOnlyList<CustomerPreference> customerPreferences, CancellationToken cancellationToken);
    
    Task<Customer> UpdateCustomerWithPreference(Customer entity, IEnumerable<CustomerPreference> customerPreferences, CancellationToken cancellationToken);
}