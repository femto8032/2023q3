﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Models.Preference;
using Otus.Teaching.PromoCodeFactory.Core.Models.PromoCode;

namespace Otus.Teaching.PromoCodeFactory.Core.Models.Customer;

public sealed record CustomerResponse
{
    public Guid Id { get; init; }
    public string FirstName { get; init; }
    public string LastName { get; init; }
    public string Email { get; init; }
        
    public ICollection<PreferenceShortResponse> Preferences { get; set; }
    public ICollection<PromoCodeShortResponse> PromoCodes { get; set; }
}