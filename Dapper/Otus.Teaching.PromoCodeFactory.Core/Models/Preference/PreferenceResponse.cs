using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Models.Customer;

namespace Otus.Teaching.PromoCodeFactory.Core.Models.Preference;

public sealed record PreferenceResponse
{
    public string Name { get; init; }
    
    public IReadOnlyCollection<CustomerShortResponse> Customers { get; init; }
}