namespace Otus.Teaching.PromoCodeFactory.Core.Models.Preference;

public sealed record PreferenceShortResponse
{
    public string Name { get; init; }
}