# Отчёт по квартальным целям Q3

## 1. Изучить механизм настройки кросс-доменных запроcов (CORS) в веб-приложении и настроить CORS в сервисе

### Механизм CORS

CORS (Cross-Origin Resource Sharing) — это стандарт, позволяющий предоставлять веб-страницам доступ к объектам сторонних интернет-ресурсов. Сторонним считается любой интернет-ресурс, который отличается от запрашиваемого протоколом, доменом или портом.

Первоначально для защиты информации была разработана «Политика одинакового источника» (Same-Origin Policy, SOP). Поддерживающий политику SOP веб-браузер сверяет комбинации сетевого протокола (например, https), точное имя домена и номер порта, чтобы разрешить доступ к ресурсам веб-страницы по запросам с другой страницы. Политика SOP не обязательна к применению, однако все современные веб-браузеры ее поддерживают.

Если веб-ресурсы интернет-источника соответствуют SOP, для доступа к ним из другого источника браузер должен поддерживать технологию Cross-Origin Resource Sharing. В 2006 году рабочая группа Консорциума Всемирной паутины (World Wide Web Consortium, W3C – организация, разрабатывающая интернет-стандарты) представила первый рабочий проект этой технологии. В 2014 году CORS был принят в качестве Рекомендации W3C.

#### Структура Cross-Origin Resource Sharing

Методы CORS предназначены для управления доступом к дескрипторам (тегам) на веб-страницах в сети. Управляемые типы доступа подразделяются на три основных категории по работе с информацией сторонних ресурсов:

1. Доступ на запись — это доступ к ссылкам, заполнению веб-форм и переадресации на сторонние веб-страницы, т.е. на передачу информации в сторонний источник (веб-ресурс).

2. Доступ на вставку относится к категории доступа на считывание информации из стороннего источника. К этому типу принадлежат вставки в код дескрипторов audio, video, img, embed, object, link, script, iframe и другие элементы оформления веб-страниц. Структура подобных дескрипторов подразумевает самостоятельную инициацию перекрестных (cross-origin) запросов из сторонних источников. Все дескрипторы этой категории представляют низкий уровень угрозы безопасности, поэтому разрешены в веб-браузере по умолчанию.

3. Доступ на считывание — это дескрипторы, загружаемые с использованием фоновых методов вызова, таких как fetch(), технологии обмена данными Ajax и пр. Поскольку подобные дескрипторы могут содержать в теле любые участки кода (в том числе вредоносного), они запрещены в веб-браузерах по умолчанию.

При настройке веб-сайта механизм CORS позволяет выборочно блокировать различные категории доступа пользователя к ресурсам (запись, вставку или считывание).

#### Принцип работы CORS

Клиент отправляет в службу с ресурсами предварительный (preflight) CORS-запрос с определенными параметрами в заголовках HTTP (CORS-заголовках, если быть точнее). Служба отвечает, используя те же заголовки с другими или такими же значениями. На основе ответа на предварительный CORS-запрос клиент решает, может ли он отправить основной запрос к службе. Браузер (клиент) выдаст ошибку, если ответ не соответствует требованиям предварительной проверки CORS.

Когда браузер отправляет запрос на сервер, он сначала отправляет Options HTTP-запрос. Это и есть предварительным CORS-запрос. Затем сервер отвечает списком разрешенных методов и заголовков. Если браузеру разрешено сделать фактический запрос, то тогда он незамедлительно отправит его. Если нет, он покажет пользователю ошибку и не выполнит основной запрос.

![Механизм CORS](https://habrastorage.org/r/w1560/getpro/habr/upload_files/add/bb3/06b/addbb306bc20bcff5a70059e336cd79e.png)

Например, страница <https://client-1.ru/page.html> запрашивает данные со страницы <https://server-site.ru/info>.
Пример запроса от браузера клиента, использующего методы CORS, приведен ниже:

``` text
GET /info HTTP/1.1
Host: server-site.ru
Origin: client-1.ru
```

В случае, если сервер по адресу <www.server-site.ru> разрешает странице-источнику запроса доступ к данным с домена, в его ответе на запрос появится строка Access-Control-Allow-Origin с именем объявленного домена:

``` text
Access-Control-Allow-Origin: https://client-1.ru
```

Если сервер, на который запрошен доступ, не добавит в ответ указанную строку, браузер клиента вместо данных файла info вернет код ошибки.

Если на сервере разрешен доступ к ресурсу страницам любого стороннего домена, в ответе будет указана маска «*».

``` text
Access-Control-Allow-Origin: *
```

Если на сервере разрешен доступ не всем, а нескольким сторонним клиентам, ответ сервера содержит имена всех этих доменов, выведенных отдельными строками или разделенных пробелами:

``` text
Access-Control-Allow-Origin: https://client-1.ru https://client-2.ru https://client-3.ru
```

### Настройка CORS на стороне бэкенда

Для контроля политики CORS используются обычные заголовки HTTP. Они используются, когда браузер отправляет предварительный CORS-запрос на сервер, на который сервер отвечает следующими заголовками:

- Access-Control-Allow-Origin указывает, какой источник может получать ресурсы. Вы можете указать один или несколько источников через запятую, например: <https://foo.io,http://bar.io>.

- Access-Control-Allow-Methods указывает, какие HTTP-методы разрешены. Вы можете указать один или несколько HTTP-методов через запятую, например: GET,PUT,POST.

- Access-Control-Allow-Headers указывает, какие заголовки запросов разрешены. Вы можете указать один или несколько заголовков через запятую, например: Authorization,X-My-Token.

- Access-Control-Allow-Credentials указывает, разрешена ли отправка файлов cookie. По умолчанию: false.

- Access-Control-Max-Age указывает в секундах, как долго должен кэшироваться результат запроса. По умолчанию: 0.

Наиболее распространенная проблема заключается в том, что браузер блокирует запрос из-за политики CORS. Браузер выдаст ошибку и отобразит в консоли следующее сообщение:

``` text
Access to XMLHttpRequest at 'http://localhost:8080/' from origin
'http://localhost:3000' has been blocked by CORS policy:
Response to preflight request doesn't pass access control
check: No 'Access-Control-Allow-Origin' header is present
on the requested resource.
```

Приведенная выше CORS-ошибка уведомляет пользователя о том, что браузер не может получить доступ к ресурсу (<https://localhost:8080>) из источника (<https://localhost:3000>), поскольку сервер его не одобрил. Это произошло из-за того, что сервер не ответил заголовком Access-Control-Allow-Origin с этим источником или символом * в ответе на предварительный CORS-запрос.

Основная идея исправления ошибок с CORS заключается в том, чтобы отвечать на OPTIONS запросы, отправленные от клиента, корректными заголовками. Для этого можно использовать прокси-сервер либо middleware на своем сервере.

Единственный способ правильно решить проблему это настройка на сервере CORS-ответов. Этого можно добиться несколькими способами из нескольких слоев приложения. Самый распространенный способ — использовать обратный прокси-сервер (reverse-proxy), API-шлюз или любой другой сервис маршрутизации, который позволяет добавлять заголовки к ответам. Для этого можно использовать множество сервисов, и вот некоторые из них: HAProxy, Linkerd, Istio, Kong, nginx, Apache, Traefik. Если инфраструктура содержит только приложение без каких-либо дополнительных слоев, то мы можем добавить поддержку CORS в код самого приложения, об этом будет рассказано ниже.

### Включение запросов CORS в ASP.NET Core

Существует три способа включения CORS:

- В middleware с помощью именованной политики или политики по умолчанию.
- Использование маршрутизации конечных точек.
- С атрибутом [EnableCors].

Прежде чем перейти к рассмотрению способов настройки CORS, попробуем протестировать кросс-доменные запросы на тестовых приложениях. Мы будем использовать два приложения - первое это учебное приложение, содержащее сервис уведомлений и принимающий запросы на получение и создание уведомлений через WebAPI ([ссылка1](https://gitlab.com/binarysquad/otus-project/-/tree/feature/TAS-64/src/BlogPlus.Backend/BlogPlus.Notification)), второе приложение будет выступать в роли клиента для нашего WebAPI приложения ([ссылка2](https://gitlab.com/femto8032/2023q3/-/tree/main/CORSClient?ref_type=heads)).

Для начала не будем настраивать CORS в приложении WebAPI и попробуем выполнить запрос со стороны клиента. При нажатии на кнопку будет выполняться ajax-запрос к приложению WebAPI, которое, будет запускаться по адресу "<http://localhost:5003/notification>". Полученный от первого приложения ответ будет загружаться в блок "div id="result"". По умолчанию в целях безопасности браузер ограничивает ajax-запросы между различными доменами, поэтому на консоле в браузере мы увидим ошибку:

![Ошибка при доступе к другому домену](https://gitlab.com/femto8032/2023q3/-/raw/main/Images/Screenshot%202023-10-03%20210107.png?ref_type=heads)

В консоли браузера видно что запрос к нашему API прошёл успешно со статусом 200:

``` text
GET http://localhost:5003/notification net::ERR_FAILED 200 (OK)
```

Однако доступ к ресурсу был ограничен браузером:

``` text
localhost/:1  Access to XMLHttpRequest at 'http://localhost:5003/notification' from origin 'http://localhost:5076' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource.
```

Теперь добавим, в наш конвейер обработки запросов, middleware CORS, который обрабатывает запросы между источниками, в качестве параметра укажем адрес нашего клиента:

``` csharp
app.UseCors(b => b.WithOrigins("http://localhost:5076"));
```

И попробуем выполнить запрос:

![Успешный запрос к другому домену](https://gitlab.com/femto8032/2023q3/-/raw/main/Images/Screenshot%202023-10-03%20213624.png?ref_type=heads)

На скриншоте видим, что запрос к нашему ресурсу прошёл успешно и данные отобразились на форме. *UseCors* должен вызываться в правильном порядке. Например, *UseCors* необходимо вызвать перед *UseResponseCaching* использованием *UseResponseCaching*. Чтобы данный компонент middleware работал, необходимо добавить его вызов между вызовами *UseRouting* и *UseEndpoints*, а также важно, чтобы в конце названия домена **не должно быть конечного слеша**. Того же самого результата можно добиться, если настроить политику CORS при добавлении сервиса через механизм внедрения зависимостей:

``` csharp
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "NotificationOrigin",
                      policy  =>
                      {
                          policy.WithOrigins("http://localhost:5076");
                      });
});
```

И затем добавив эту политику в конвеер обработки запросов:

``` csharp
app.UseCors("NotificationOrigin");
```

С помощью CorsPolicyBuilder мы можем настроить CORS, следующими методами:

- *AllowAnyOrigin()*: принимаются запросы с любого адреса

- *AllowAnyHeader()*: принимаются запросы с любыми заголовками

- *AllowAnyMethod()*: принимаются запросы любого типа (GET/POST)

- *AllowCredentials()*: разрешается принимать идентификационные данные от клиента (например, куки)

- *WithHeaders()*: принимаются только те запросы, которые используют содержат определенные заголовки

- *WithMethods()*: принимаются запросы только определенного типа

- *WithOrigins()*: принимаются запросы только с определенных адресов

- *WithExposedHeaders()*: позволяет серверу отправлять на сторону клиента свои заголовки

Проверим работу метода *WithMethods()*, для этого добавим в наше приложение ещё одну кнопку, которая при нажатии будет отправлять POST запрос к API:

![Post запрос](https://gitlab.com/femto8032/2023q3/-/raw/main/Images/Screenshot%202023-10-03%20222804.png?ref_type=heads)

На скриншоте выше мы видим, что запрос прошёл успешно, ошибок с CORS не было. Теперь попробуем добавить ограничение на метод и разрешить только GET запросы.

``` csharp
    app.UseCors(b => b
        .WithOrigins("http://localhost:5076")
        .WithMethods("GET")
        .AllowAnyHeader());
```

Выполним оба запроса:

![Post запрос](https://gitlab.com/femto8032/2023q3/-/raw/main/Images/Screenshot%202023-10-03%20223730.png?ref_type=heads)

Здесь мы видим, что GET запрос прошёл успешно, а при выполнении POST запроса мы получили ошибку в консоле:

``` text
localhost/:1  Access to XMLHttpRequest at 'http://localhost:5003/notification' from origin 'http://localhost:5076' has been blocked by CORS policy: Request header field content-type is not allowed by Access-Control-Allow-Headers in preflight response.
```

В примерах выше мы настраивали CORS с помощью middleware для всех запросов, но если нам необходимо, то мы можем настраивать его для определённых запросов или контроллеров. Для того, что бы настроить CORS для определённого метода в minimal api, мы можем использовать методы расширения *RequireCors*, для нашего API это будет выглядеть так:

``` csharp
    app.MapGet("/notification", async (
                [AsParameters] NotificationRequest request,
                [FromServices] INotificationRepository repository,
                HttpContext context)
            => await repository.Find(request, context.RequestAborted)
                is { } notificationModel
                ? Results.Ok(notificationModel)
                : Results.NotFound())
        .WithOpenApi()
        .RequireCors("MyCorsPolicy");
```

Если мы используем контроллеры, то для настройки CORS мы можем воспользоваться атрибутом [EnableCors], его можно применить ко всему контроллеры, либо к определённому методу:

``` csharp
[EnableCors("MyCorsPolicy")]
[Route("api/[controller]")]
[ApiController]
public class ValuesController : ControllerBase
{
    // GET api/values
    [HttpGet]
    public IActionResult Get() =>
        ControllerContext.MyDisplayRouteInfo();

    // GET api/values/5
    [HttpGet("{id}")]
    public IActionResult Get(int id) =>
        ControllerContext.MyDisplayRouteInfo(id);
}
```

``` csharp
[Route("api/[controller]")]
[ApiController]
public class ValuesController : ControllerBase
{
    // GET api/values
    [HttpGet]
    [EnableCors("MyCorsPolicy")]
    public IActionResult Get() =>
        ControllerContext.MyDisplayRouteInfo();

    // GET api/values/5
    [HttpGet("{id}")]
    public IActionResult Get(int id) =>
        ControllerContext.MyDisplayRouteInfo(id);
}
```

Для **отключения** CORS необходимо использовать аттрибут [DisableCors]:

``` csharp
[EnableCors("MyCorsPolicy")]
[Route("api/[controller]")]
[ApiController]
public class ValuesController : ControllerBase
{
    // GET api/values
    [HttpGet]
    public IActionResult Get() =>
        ControllerContext.MyDisplayRouteInfo();


    // GET: api/values/GetValues2
    [DisableCors]
    [HttpGet("{action}")]
    public IActionResult GetValues2() =>
        ControllerContext.MyDisplayRouteInfo();

}
```

Если сервер отправляет какие-то свои заголовки, то по умолчанию клиент их не получает. Чтобы на стороне сервера указать, какие заголовки может получать клиент, следует использовать метод WithExposedHeaders():

``` csharp
builder.Services.AddCors(options =>
{
    options.AddPolicy("MyCorsPolicy",
        policy =>
        {
            policy.WithOrigins("http://localhost:5076")
                  .WithExposedHeaders("x-custom-header");;
        });
});
```

По умолчанию браузер не отправляет учетные данные(cookie, схемы проверки подлинности HTTPS) в запросе между источниками. Чтобы отправить учетные данные в запросе, на клиенте должно быть задано значение XMLHttpRequest.withCredentialstrue. Сервер должен разрешить принять учетные данные, для этого необходимо вызвать AllowCredentials:

``` csharp
builder.Services.AddCors(options =>
{
    options.AddPolicy("MyCorsPolicy",
        policy =>
        {
            policy.WithOrigins("http://localhost:5076")
                   .AllowCredentials();
        });
});
```

На этом отчёт по данной целе закончен. Полезные ссылки:

- [Учебное приложение1](https://gitlab.com/binarysquad/otus-project/-/tree/feature/TAS-64/src/BlogPlus.Backend/BlogPlus.Notification?ref_type=heads) и [Учебное приложение2](https://gitlab.com/femto8032/2023q3/-/tree/main/CORSClient?ref_type=heads)
- [Офф. документация по настройке CORS](https://learn.microsoft.com/ru-ru/aspnet/core/security/cors?view=aspnetcore-7.0)
- [Цикл статей на metanit по CORS](https://metanit.com/sharp/aspnet5/31.1.php)
- [Статья на developer.mozilla.org](https://developer.mozilla.org/ru/docs/Web/HTTP/CORS)
- [Статья на хабре](https://habr.com/ru/articles/335772/)

## 2. Реализовать сервис с использованием кластера NoSQL БД MongoDB и настроить механизм репликации

### Введение

В контексте MongoDB “*кластер*” - это слово, которое используется для обозначения набора реплик (*[replica set](https://www.mongodb.com/basics/replication)*), либо для сегментированного кластера (*[sharded cluster](https://www.mongodb.com/docs/manual/sharding/)*). В данной квартальной цели рассматривается механизм репликации с помощью *replica set* и взаимодействие с набором реплик в среде .NET.

*MongoDB* достигает репликации с помощью **набора реплик**. Набор реплик – это группа экземпляров **mongod**, которые содержат один и тот же набор данных. Репликация защищает базу данных от потери одного сервера. Репликация также позволяет восстанавливаться после сбоя оборудования и перерывов в обслуживании. В дополнение к отказоустойчивости, наборы реплик также могут обеспечить дополнительную пропускную способность для операций чтения для кластера *MongoDB*, перенаправляя клиентов на дополнительные серверы, что впоследствии снижает общую нагрузку на кластер. Наборы реплик также могут быть полезны для распределенных приложений из-за предлагаемой ими локальности данных, так что более быстрые и параллельные операции чтения выполняются с наборами реплик вместо того, чтобы проходить через один главный сервер.

### Механизм репликации

![Механизм репликации](https://webimages.mongodb.com/_com_assets/cms/kseryedf3pajb7i31-mongodb-replicaset.png)

Кластер MongoDB должен иметь **первичный узел** и **набор вторичных узлов**, чтобы считаться набором реплик. Один узел является основным узлом, который получает все операции записи. Все изменения в наборах данных основного узла записываются в специальную ограниченную коллекцию, называемую **журналом операций** (oplog).

Роль вторичных узлов заключается в том, чтобы затем реплицировать журнал операций первичного узла и убедиться, что наборы данных точно отражают наборы данных первичного узла. Эта функциональность проиллюстрирована на следующей диаграмме:

![Схема репликации](https://webimages.mongodb.com/_com_assets/cms/kkh4ocykxj5dz430q-mongodb-replica-sets.png)

Репликация из oplog первичного узла во вторичные происходит **асинхронно**, что позволяет набору реплик продолжать функционировать, несмотря на потенциальный сбой одного или нескольких его элементов. Если текущий основной узел внезапно становится недоступным, будет **произведен выбор** для определения нового основного узла. В сценарии с двумя вторичными узлами один из вторичных узлов станет основным узлом:

![Схема выбора основного узла](https://webimages.mongodb.com/_com_assets/cms/kses274yc8v1e1ysq-mongodb-replicaset-primary-failure.png)

Если по какой-либо причине существует только один вторичный узел, к набору реплик может быть добавлен специальный экземпляр с именем **arbiter**, который может участвовать только в выборах набора реплик, но сам он не реплицирует oplog из первичного. Это означает, что данный экземпляр не может обеспечить избыточность данных и всегда будет арбитром, т.е. он **не может стать первичным или вторичным узлом**, тогда как первичный может стать вторичным узлом и наоборот.

### Операции чтения и записи

Набор реплик в кластере MongoDB прозрачен для клиентских приложений. Это означает, что они не могут определить, включен ли в кластере набор реплик или он запущен при развертывании на одном сервере.

Однако MongoDB предлагает дополнительные операции чтения и записи. Клиентские приложения могут напрямую обращаться к узлу набора реплик, на котором должна выполняться операция чтения. **По умолчанию все операции чтения направляются на первичный узел**, но также можно настроить конкретную маршрутизацию на вторичные узлы; это называется предпочтением чтения.

Можно настроить несколько **режимов предпочтения** ([Read Preference](https://www.mongodb.com/docs/manual/core/read-preference/#nearest)) для операции чтения. Например, если клиентское приложение настроено на прямой доступ к вторичным ресурсам, то параметру *mode* в настройках чтения должно быть присвоено значение *secondary*. Если существуют потребности в наименьшей задержке в сети, независимо от того, происходит ли это на первичном или любом другом вторичном узле, то следует настроить режим *nearest*. Однако в этом варианте возникает риск потенциально устаревших данных (если ближайший узел является вторичным) из-за природы асинхронной репликации от первичного к вторичным.

![Read Preference](https://www.mongodb.com/docs/manual/images/replica-set-read-preference-secondary.bakedsvg.svg)

В качестве альтернативы, режим предпочтения чтения может быть установлен на *primary preferred* или *secondary preferred*. Эти два режима также используют свойство, называемое *maxStalenessSeconds* чтобы определить, с какого узла нам следует выполнять операцию чтения. Во всех случаях, когда существует вероятность того, что операция чтения произойдет на неосновном узле, мы должны убедиться, что приложение может переносить устаревшие данные.

### Настройка и установка ReplicaSet

В данном разделе будет описана процедура локального развёртывания в Docker. Для того чтобы развернуть кластер  нам необходим установленный Docker и докер образ MongoDB Community server.

1. Первым шагом нам необходимо создать Docker network, чтобы каждый из запущенных контейнеров с экземпляром MongoDB, мог видеть друг друга.

    ``` docker
    docker network create mongoCluster
    ```

    Параметром mongoCluster здесь является название сети. После выполнения команды мы можем увидеть идентификатор сети, которую только что создали.

2. Далее необходимо запустить первый контейнер с MongoDB следующей командой:

    ``` docker
    docker run -d --rm -p 27017:27017 --name mongo1 --network mongoCluster mongo:5 mongod --replSet myReplicaSet --bind_ip localhost,mongo1
    ```

    Инструкция после *mongod* будет выполнена после запуска контейнера. Эта команда создает новый экземпляр mongod, готовый к включению в набор реплик *myReplicaSet*.

    Если команда была успешно выполнена, мы увидим длинную шестнадцатизначную строку, представляющую идентификатор контейнера. Далее необходимо запустить два других контейнера, с другим именем и другим портом, но в той же сети которую мы создали на шаге 1.

     ``` docker
    docker run -d --rm -p 27018:27017 --name mongo2 --network mongoCluster mongo:5 mongod --replSet myReplicaSet --bind_ip localhost,mongo2
 
    docker run -d --rm -p 27019:27017 --name mongo3 --network mongoCluster mongo:5 mongod --replSet myReplicaSet --bind_ip localhost,mongo3
    ```

    Если всё прошло успешно мы должны увидеть три запущенных контейнера, с проброшенными портами на 27017 порт.

    ![Созданные контейнеры](https://gitlab.com/femto8032/2023q3/-/raw/main/Images/Screenshot%202023-09-30%20172404.png?ref_type=heads)

3. Необходимо проинициализировать Replica Set, т.е. создать кластер и добавить в него узлы созданные на предыдущем шаге, в *host* мы указываем имя контейнера.

    ``` docker
    docker exec -it mongo1 mongosh --eval "rs.initiate({
    _id: \"myReplicaSet\",
    members: [
        {_id: 0, host: \"mongo1\"},
        {_id: 1, host: \"mongo2\"},
        {_id: 2, host: \"mongo3\"}
     ]
    })"
    ```

    Данной командой мы создаём набор реплик с именем *"myReplicaSet"* и узлами, которые являются докер контейнерами. Проверяем статус созданной реплики, на консоле должна появиться информация о реплике и primary узле:

    ``` docker
    docker exec -it mongo1 mongosh --eval "rs.status()"
    ```

    На консоле должен отобразиться статус реплики подобного вида:

    ``` docker
    Current Mongosh Log ID: 65182b5b183f3c85dd2c75c8
    Connecting to:          mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+2.0.0
    Using MongoDB:          5.0.21
    Using Mongosh:          2.0.0

    For mongosh info see: https://docs.mongodb.com/mongodb-shell/


    To help improve our products, anonymous usage data is collected and sent to MongoDB periodically (https://www.mongodb.com/legal/privacy-policy).
    You can opt-out by running the disableTelemetry() command.

    ------
    The server generated these startup warnings when booting
    2023-09-30T13:23:42.214+00:00: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine. See http://dochub.mongodb.org/core/prodnotes-filesystem
    2023-09-30T13:23:42.809+00:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
    2023-09-30T13:23:42.809+00:00: /sys/kernel/mm/transparent_hugepage/enabled is 'always'. We suggest setting it to 'never'
    ------

    {
        set: 'myReplicaSet',
        date: ISODate("2023-09-30T14:06:19.261Z"),
        myState: 1,
        term: Long("4"),
        syncSourceHost: '',
        syncSourceId: -1,
        heartbeatIntervalMillis: Long("2000"),
        majorityVoteCount: 2,
        writeMajorityCount: 2,
        votingMembersCount: 3,
        writableVotingMembersCount: 3,
        optimes: {
            lastCommittedOpTime: { ts: Timestamp({ t: 1696082774, i: 1 }), t: Long("4") },
            lastCommittedWallTime: ISODate("2023-09-30T14:06:14.794Z"),
            readConcernMajorityOpTime: { ts: Timestamp({ t: 1696082774, i: 1 }), t: Long("4") },
            appliedOpTime: { ts: Timestamp({ t: 1696082774, i: 1 }), t: Long("4") },
            durableOpTime: { ts: Timestamp({ t: 1696082774, i: 1 }), t: Long("4") },
            lastAppliedWallTime: ISODate("2023-09-30T14:06:14.794Z"),
            lastDurableWallTime: ISODate("2023-09-30T14:06:14.794Z")
        },
        lastStableRecoveryTimestamp: Timestamp({ t: 1696082734, i: 1 }),
        electionCandidateMetrics: {
        lastElectionReason: 'electionTimeout',
        lastElectionDate: ISODate("2023-09-30T13:23:54.718Z"),
        electionTerm: Long("4"),
        lastCommittedOpTimeAtElection: { ts: Timestamp({ t: 0, i: 0 }), t: Long("-1") },
        lastSeenOpTimeAtElection: { ts: Timestamp({ t: 1695932339, i: 2 }), t: Long("2") },
        numVotesNeeded: 2,
        priorityAtElection: 1,
        electionTimeoutMillis: Long("10000"),
        numCatchUpOps: Long("0"),
        newTermStartDate: ISODate("2023-09-30T13:23:54.724Z"),
        wMajorityWriteAvailabilityDate: ISODate("2023-09-30T13:23:55.271Z")
    },
    members: [
    {
      _id: 0,
      name: 'mongo1:27017',
      health: 1,
      state: 1,
      stateStr: 'PRIMARY',
      uptime: 2557,
      optime: { ts: Timestamp({ t: 1696082774, i: 1 }), t: Long("4") },
      optimeDate: ISODate("2023-09-30T14:06:14.000Z"),
      lastAppliedWallTime: ISODate("2023-09-30T14:06:14.794Z"),
      lastDurableWallTime: ISODate("2023-09-30T14:06:14.794Z"),
      syncSourceHost: '',
      syncSourceId: -1,
      infoMessage: '',
      electionTime: Timestamp({ t: 1696080234, i: 1 }),
      electionDate: ISODate("2023-09-30T13:23:54.000Z"),
      configVersion: 1,
      configTerm: 4,
      self: true,
      lastHeartbeatMessage: ''
    },
    {
      _id: 1,
      name: 'mongo2:27017',
      health: 1,
      state: 2,
      stateStr: 'SECONDARY',
      uptime: 2554,
      optime: { ts: Timestamp({ t: 1696082774, i: 1 }), t: Long("4") },
      optimeDurable: { ts: Timestamp({ t: 1696082774, i: 1 }), t: Long("4") },
      optimeDate: ISODate("2023-09-30T14:06:14.000Z"),
      optimeDurableDate: ISODate("2023-09-30T14:06:14.000Z"),
      lastAppliedWallTime: ISODate("2023-09-30T14:06:14.794Z"),
      lastDurableWallTime: ISODate("2023-09-30T14:06:14.794Z"),
      lastHeartbeat: ISODate("2023-09-30T14:06:18.780Z"),
      lastHeartbeatRecv: ISODate("2023-09-30T14:06:17.763Z"),
      pingMs: Long("0"),
      lastHeartbeatMessage: '',
      syncSourceHost: 'mongo1:27017',
      syncSourceId: 0,
      infoMessage: '',
      configVersion: 1,
      configTerm: 4
    },
    {
      _id: 2,
      name: 'mongo3:27017',
      health: 1,
      state: 2,
      stateStr: 'SECONDARY',
      uptime: 2554,
      optime: { ts: Timestamp({ t: 1696082774, i: 1 }), t: Long("4") },
      optimeDurable: { ts: Timestamp({ t: 1696082774, i: 1 }), t: Long("4") },
      optimeDate: ISODate("2023-09-30T14:06:14.000Z"),
      optimeDurableDate: ISODate("2023-09-30T14:06:14.000Z"),
      lastAppliedWallTime: ISODate("2023-09-30T14:06:14.794Z"),
      lastDurableWallTime: ISODate("2023-09-30T14:06:14.794Z"),
      lastHeartbeat: ISODate("2023-09-30T14:06:18.780Z"),
      lastHeartbeatRecv: ISODate("2023-09-30T14:06:17.761Z"),
      pingMs: Long("0"),
      lastHeartbeatMessage: '',
      syncSourceHost: 'mongo1:27017',
      syncSourceId: 0,
      infoMessage: '',
      configVersion: 1,
      configTerm: 4
        }
    ],
    ok: 1,
    '$clusterTime': {
        clusterTime: Timestamp({ t: 1696082774, i: 1 }),
        signature: {
            hash: Binary.createFromBase64("AAAAAAAAAAAAAAAAAAAAAAAAAAA=", 0),
            keyId: Long("0")
            }
        },
        operationTime: Timestamp({ t: 1696082774, i: 1 })
    }
    ```

    Если команда **не сработала** можно попробовать выполнить её напрямую в контейнере первичного узла, для этого переходим в контейнер и выполняем комманду:

    ``` bash
    mongo
    ```

    И затем в MongoShell выполняем команду с инициализацией:

    ``` json
    rs.initiate(
    {
        _id: "myReplicaSet",
        members: [
            {_id: 0, host: "mongo1"},
            {_id: 1, host: "mongo2"},
            {_id: 2, host: "mongo3"}
     ]
    })
    ```

    В *members* мы также можем указать удалённый хост если таковой у нас имеется *"host : ip:27017"*

    Далее проверяем конфигурацию:

     ``` bash
    rs.conf()
    ```

    Сразу после добавления в реплику узел будет в статусе 5, это означает что он присоединился к реплике и в данный момент синхронизируется.

На данном этапе мы создали реплику с тремя узлами и уже можем работать с ней. Также опционально мы можем добавить узлы вручную если необходимо:

``` bash
rs.add("<Secondary server ip>:27017")
```

При добавлении узла в уже существующую реплику она какое-то время будет недоступна в связи с синхронизацией. Либо можем добавить арбитра:

``` bash
rs.addArb("<arbiter server ip>:27017")
```

Статусы узлов в rs.status():

1 — Primary

2 — Secondary

7 — Arbiter

Либо можем настроить приоритеты для узлов, чем выше цифра приоритета, тем ниже сам приоритет при выборе Primary узла.

``` bash
cfg = rs.conf()
cfg.members[0].priority = 2 
cfg.members[1].priority = 3       
cfg.members[2].priority = 1 
rs.reconfig(cfg, {force : true})
```

На этом конфигурацию реплики можно считать завершённой, более подробно про конфигурацию можно почитать по [ссылке](https://www.mongodb.com/docs/manual/reference/replica-configuration/).

### Работа с репликой со стороны клиента

Для проверки созданной реплики попробуем подключиться к ней с помощью строки подключения, из любого клиента. Для подключения используется [строка подключения](https://www.mongodb.com/docs/manual/reference/connection-string/) и  в ней указываются адреса всех её нод и имя реплики, пример строки подключения:

``` bash
mongodb+srv://myDatabaseUser:D1fficultP%40ssw0rd@mongodb0.example.com:27017,mongodb1.example.com:27017,mongodb2.example.com:27017/?authSource=admin&replicaSet=myRepl
```

В нашем случае, узлы были развёрнуты как докер контейнеры с одним портом, и ОС не сможет найти наши хосты, поэтому мы можем либо добавить их в *etc/hosts* либо подключиться к реплике без указания хостов как указано ниже:

``` bash
mongodb://localhost:27017/dbtest?replicaSet=myReplicaSet
```

При подключении через MongoDB Compass мы увидим следующее:

![Созданные контейнеры](https://gitlab.com/femto8032/2023q3/-/raw/main/Images/Screenshot%202023-09-30%20184730.png?ref_type=heads)

Подключившись мы видим, что подключены к кластеру и у нас есть три узла, так же указаны адреса наших узлов.

Далее попробуем подключиться к кластеру из .NET приложения. В рамках данной цели MongoDB использовалась в учебном проекте доступном по [ссылке](https://gitlab.com/binarysquad/otus-project/-/tree/feature/TAS-64/src/BlogPlus.Backend/BlogPlus.Notification?ref_type=heads), здесь MongoDB используется для хранения отправленных уведомлений.

Для того чтобы работать с MongoDB в среде .NET нам необходимо в приложение установить официальный [nuget-пакет](https://www.nuget.org/profiles/MongoDB) с драйвером. После создания модели и добавления репозитория мы можем работать с клиентом Mongo. Конструктор *MongoClient* может принимать три параметра, первый это connectionString в виде строки, ниже приведена строка подключения к .NET приложению.

``` bash
mongodb://localhost:27017/NotificationDB?replicaSet=myReplicaSet
```

В ней после префикса указывается хост либо список хостов, на которых расположены ноды, и тогда префикс будет - *mongodb+srv*. Далее указываем имя БД, если её не существует, то она будет создана. Затем идут опции которые не обязательно указывать, но если мы подключаемся к реплике, то нужно указать её имя. Из опций для реплики, можно указать например ноду с которой мы предпочитаем читать данные, в параметре - *readPreference*, тогда строка подключения будет выглядеть так:

``` bash
mongodb://localhost:27017/NotificationDB?replicaSet=myReplicaSet&readPreference=primary
```

Также доступны опции [пула подключений](https://www.mongodb.com/docs/manual/reference/connection-string/#connection-pool-options), [сжатия](https://www.mongodb.com/docs/manual/reference/connection-string/#compression-options), [аутентификации](https://www.mongodb.com/docs/manual/reference/connection-string/#authentication-options) и другие. Опции, после подключения через строку, будут доступны в *MongoClient*, в свойстве ***Settings***.

Вторым способ подключения является MongoUrl. И третьим является класс MongoClientSettings в котором задаются те же параметры, что и в строке подключения. Так будет выглядеть наша строка подключения в виде MongoClientSettings:

``` csharp
var settings = new MongoClientSettings
{
    Server = new MongoServerAddress("mongodb://localhost:27017"),
    DirectConnection = false,
    ReadPreference = ReadPreference.Primary
};
```

Если мы подключаемся к реплике, то параметр DirectConnection должен быть false, также мы можем указать список хостов с нодами:

``` csharp
var settings = new MongoClientSettings
{
    Servers = new MongoServerAddress[]
    {
        new ("mongodb://mongo1:27017"),
        new ("mongodb://mongo2:27018"),
        new ("mongodb://mongo3:27019")
    },
    DirectConnection = false,
    ReadPreference = ReadPreference.Primary
};
```

На этом отчёт по данной целе закончен. Полезные ссылки:

- [Учебное приложение](https://gitlab.com/binarysquad/otus-project/-/tree/feature/TAS-64/src/BlogPlus.Backend/BlogPlus.Notification?ref_type=heads)
- [Офф. документация про репликацию](https://www.mongodb.com/docs/manual/replication/)
- [Офф. документация про настройку репликацию](https://www.mongodb.com/basics/clusters/mongodb-cluster-setup)
- [Офф. документация с примером разворачивания в докерк](https://www.mongodb.com/compatibility/deploying-a-mongodb-cluster-with-docker)
- [Статья на хабре](https://habr.com/ru/articles/335772/)

## 3. Реализовать репозиторий в сервисе с использованием ORM Dapper

### Обзор ORM Dapper

Dapper - это (ORM) с открытым исходным кодом для платформы .NET. Библиотека позволяет легко и быстро получать доступ к данным из БД, а также выполнять необработанные SQL-запросы, сопоставлять результаты из БД с объектами приложения и выполнять хранимые процедуры. Он доступен в виде пакета NuGet.

Dapper легкий и быстрый, что делает его идеальным выбором для приложений, требующих низкой задержки и высокой производительности. Он позволяет быстро и легко сопоставлять результаты запросов ADO.NET data readers с экземплярами бизнес-объектов.
Также Dapper обладает поддержкой как асинхронных, так и синхронных запросов к базе данных, объединения нескольких запросов в один вызов и  поддерживает параметризованные запросы для защиты от атак с использованием SQL-инъекций.

Основная причина использовать Dapper это производительность. Изначально разработчики Dapper(команда Stack Overflow) использовали Entity Framework Core, однако они обнаружили, что производительность запросов была недостаточно высокой для увеличения трафика, который испытывал их сайт, поэтому они написали свой собственный микро-ORM.

Таким образом, Dapper является хорошим выбором в сценариях, где данные часто меняются и часто запрашиваются. Dapper работает с ADO.NET  IDbConnection, что означает, что он будет работать с любой СУБД, для которой существует провайдер ADO.NET. Также его можно использовать в одном проекте с другим ORM.

#### Сравнение Dapper и Entity Framework

Dapper представляет собой micro-ORM, т.е. легковесной и ограниченной по функциональности, версией классического ORM такого как EF Core. Основные отличия ORM и micro-ORM продемонстрированы ниже:

![Сравнение ORM и micro-ORM](https://gitlab.com/femto8032/2023q3/-/raw/main/Images/Screenshot%202023-10-05%20002403.png?ref_type=heads)

Dapper и EF Core имеют некоторое различие в дизайне и подходе, что делает их подходящими для своих вариантов использования. Вот несколько факторов, которые следует учитывать при выборе между Dapper и EF Core.

- Производительность.
Dapper, как правило, быстрее, чем EF Core, поскольку он использует необработанные SQL-запросы и имеет меньшие накладные расходы. Однако EF Core обеспечивает кэширование и другие оптимизации производительности, которые иногда могут ускорить запрос.

- Сложность.
Dapper проще, чем EF Core, потому что он использует необработанные SQL-запросы и обладает меньшим количеством функций. Однако EF Core обеспечивает абстракцию более высокого уровня над базой данных и обладает большим количеством функций.

- Поддержка баз данных.
Dapper поддерживает множество баз данных, включая SQL Server, MySQL и PostgreSQL. Однако EF Core поддерживает еще больше баз данных, включая SQLite, Oracle и IBM DB2.

- Гибкость.
Dapper обеспечивает большую гибкость, чем EF Core, поскольку позволяет выполнять необработанные SQL-запросы и сопоставлять результаты с объектами. Однако EF Core обеспечивает более структурированный подход к доступу к данным, который может упростить работу с ними в более крупных проектах.

### Возможности Dapper

В рамках данной квартальной цели, для демонстрации возможностей Dapper, было создано учебное приложение доступное по ссылке ([приложение](https://gitlab.com/femto8032/2023q3/-/tree/main/Dapper?ref_type=heads)). В данном приложении реализован веб сервис, который взаимодействует с базой данных через Dapper. С помощью методов контроллера CustomersController мы можем управлять сущностью Customer, которая связана с сущностью PromoCode связью one-to-many и сущностью Preference связью many-to-many, как показано ниже на рисунке:

![Схема БД](https://gitlab.com/femto8032/2023q3/-/raw/main/Images/Screenshot%202023-10-05%20005020.png?ref_type=heads)

Для работы с Dapper нам необходимо установить в приложение ([nuget пакет](https://www.nuget.org/packages/Dapper/)) и подключить провайдера для нашей СУБД. Так как у нас используется PostgreSQL, нам необходимо установить [Npgsql](https://www.npgsql.org/), используя данное подключение мы будем отправлять запросы к БД.

Начнём рассмотрение возможностей Dapper с читающих запросов. Запрос данных с помощью Dapper довольно прост. Нам необходимо указать select  запрос и параметры для него, если они есть, и после этого Dapper автоматически сопоставит результирующие столбцы с их соответствующими свойствами в нашей модели.

``` csharp
// Connect to the database 
using (var connection = new SqlConnection(connectionString)) 
{    
    // Create a query that retrieves all books with an author name of "John Smith"    
    var sql = "SELECT * FROM Books WHERE Author = @authorName";     

    // Use the Query method to execute the query and return a list of objects    
    var books = connection
        .Query<Book>(sql, new { authorName = "John Smith" })
        .ToList(); 
}
```

Для выборки Customer select запросом наш код будет выглядеть так:

``` csharp
// Получение списка Customer
using (var connection = new SqlConnection(connectionString)) 
{    
        await using var sqlConnection = _connectionFactory.CreateSqlConnection();

        var result = await sqlConnection
            .QueryAsync<CustomerShortResponse>(
                new CommandDefinition(
                    """
                    SELECT c."Id", c."FirstName", c."LastName", c."Email"
                    FROM public."Customers" c
                    """,
                    cancellationToken: cancellationToken));
        
        return result.ToList();
}
```

Чтобы использовать select запрос нам необходимо у нашего IDbConnection(в случае с PostgreSQL это NpgsqlConnection) вызвать метод расширения Query либо QueryAsync, и передать в качестве параметра либо комманду sql и параметры запроса, либо CommandDefinition. Если запрос выполняется без параметров их можно не указывать, однако если они есть их лучше использовать параметризованный запрос, это повышает безопасность запроса.

При создании параметризованного запроса важно использовать правильный тип данных при привязке значений.
Например, если столбец является целым числом, нам следует использовать int или Int32, а не строку. Это гарантирует, что база данных будет обрабатывать значение как правильный тип данных, что снижает вероятность возникновения ошибки.
Это также помогает повысить производительность, гарантируя, что в базу данных передаются только допустимые значения.

Если нам необходимо получить скалярное значение, это могут быть такие запросы, как извлечение общего количества строк в таблице базы данных или суммы значений из нескольких столбцов, мы можем воспользоваться методом ExecuteScalar, который часто используется для выполнения SQL-запросов, которые возвращают одну строку и один столбец, например:

``` csharp
using (var connection = new SqlConnection(connectionString))
{
    var sql = "SELECT COUNT(*) FROM Products";
    var count = connection.ExecuteScalar(sql);

    Console.WriteLine($"Total products: {count}");
}
```

В нашем репозитории мы можем проверить существование Customer с помощью такого запроса:

``` csharp
await using var sqlConnection = _connectionFactory.CreateSqlConnection();
return await sqlConnection.ExecuteScalarAsync<bool>(
        new CommandDefinition(
            """SELECT EXISTS (SELECT 1 FROM public."Customers" 
            WHERE "Customers"."Id" = @id)""", 
            new { id }, 
            cancellationToken: cancellationToken));
```

Таким образом нам нет необходимости получать всю строку Customer, если нам необходимо всего лишь проверить его наличие по идентификатору.

Если нам необходимо получить единственную строку мы можем воспользоваться методами QuerySingle / QuerySingle< T > и QueryFirst / QueryFirst< T > и их асинхронные версии.

``` csharp
using (var connection = new SqlConnection(connectionString))
{
    var sql = "SELECT * FROM Products WHERE ProductID = 1";
    var product = connection.QuerySingle(sql);

    Console.WriteLine($"{product.ProductID} {product.ProductName}");
}
```

В нашем репозитории разберём более сложный пример:

``` csharp
        await using var sqlConnection = _connectionFactory.CreateSqlConnection();

        var lookup = new Dictionary<Guid, CustomerResponse>();
        await sqlConnection
            .QueryAsync<CustomerResponse, PromoCodeShortResponse, PreferenceShortResponse, CustomerResponse>(
                new CommandDefinition(
                    """
                    SELECT c."Id", c."FirstName", c."LastName", c."Email", pc."Id", pc."Code", pc."ServiceInfo", pc."BeginDate", pc."EndDate", pc."PartnerName", p."Name"
                    FROM public."Customers" c
                    INNER JOIN public."PromoCodes" pc ON pc."CustomerId" = c."Id"
                    INNER JOIN "CustomerPreferences" cp ON cp."CustomerId" = c."Id"
                    INNER JOIN "Preferences" p ON p."Id"  = cp."PreferenceId"
                    WHERE c."Id" = @id
                    """,
                    new { id },
                    cancellationToken: cancellationToken),
                (customer, code, preference) =>
                {
                    if (!lookup.TryGetValue(customer.Id, out var course)) 
                        lookup.Add(customer.Id, course = customer);
                    
                    course.PromoCodes ??= new List<PromoCodeShortResponse>();
                    course.PromoCodes.Add(code);
                    
                    course.Preferences ??= new List<PreferenceShortResponse>();
                    course.Preferences.Add(preference);
                    
                    return course;
                },
                $"{nameof(Customer.Id)}, {nameof(Preference.Name)}");
        
        return lookup[id];
```

Допустим нам необходимо получить только данные о Customer, только с определённых столбцов и сопоставить их с полями нашей ДТО CustomerResponse, которое содержит в себе ещё две коллекции, которые связаны с Customer связями one-to-many и many-to-many. Так выглядит данная ДТО:

``` csharp
public sealed record CustomerResponse
{
    public Guid Id { get; init; }
    public string FirstName { get; init; }
    public string LastName { get; init; }
    public string Email { get; init; }
        
    public ICollection<PreferenceShortResponse> Preferences { get; set; }
    public ICollection<PromoCodeShortResponse> PromoCodes { get; set; }
}
```

И вложенные в неё коллекции:

``` csharp
public sealed record PreferenceShortResponse
{
    public string Name { get; init; }
}
```

``` csharp
public sealed record PromoCodeShortResponse
{
    public Guid Id { get; init; }
        
    public string Code { get; init; }

    public string ServiceInfo { get; init; }

    public string BeginDate { get; init; }

    public string EndDate { get; init; }

    public string PartnerName { get; init; }
}
```

Начнём разбор нашего запроса с данных, которые нам необходимо получить.  Multi-mapping реализован как общий делегат функции. Существуют перегрузки методов Query и QueryAsync, которые принимают несколько общих параметров и аргумент Func< TFirst, ..., TReturn >. Последний параметр представляет возвращаемый тип, в то время как остальные являются входными параметрами, которые должны быть обработаны в теле делегата Func, который является функцией маппинга.

``` csharp
await sqlConnection.QueryAsync<CustomerResponse, PromoCodeShortResponse, PreferenceShortResponse, CustomerResponse>
```

Инициализируем CommandDefinition и после оператора SELECT мы указываем столбцы, которые используются в наших ДТО.

``` sql
SELECT c."Id", c."FirstName", c."LastName", c."Email", pc."Id", pc."Code", pc."ServiceInfo", pc."BeginDate", pc."EndDate", pc."PartnerName", p."Name"
```

Далее с помощью JOIN нам надо соединить наши таблицы по внешним ключам.

``` sql
FROM public."Customers" c
INNER JOIN public."PromoCodes" pc ON pc."CustomerId" = c."Id"
INNER JOIN "CustomerPreferences" cp ON cp."CustomerId" = c."Id"
INNER JOIN "Preferences" p ON p."Id"  = cp."PreferenceId"
```

И затем указываем условие выборки.

``` sql
WHERE c."Id" = @id
```

Следующим параметром в CommandDefinition указываем значение Id нашего Customer.

``` csharp
new { id },
```

Функция маппинга определяет, как результирующие данные должны быть сопоставлены с типом возвращаемого значения, здесь настраиваем параметры маппинга:

``` csharp
(customer, code, preference) =>
{
    if (!lookup.TryGetValue(customer.Id, out var course)) 
        lookup.Add(customer.Id, course = customer);
                    
    course.PromoCodes ??= new List<PromoCodeShortResponse>();
    course.PromoCodes.Add(code);
                    
    course.Preferences ??= new List<PreferenceShortResponse>();
    course.Preferences.Add(preference);
                    
    return course;
},
```

Параметры делегаты будут тех типов, которые мы указали в QueryAsync<>. Так как мы будем получать строки из БД, при наличии нескольких PromoCodes или Preferences у Customer, нам придут несколько строк.

![Полученные строки](https://gitlab.com/femto8032/2023q3/-/raw/main/Images/Screenshot%202023-10-05%20015140.png?ref_type=heads)

Поэтому нам надо сгруппировать их по Customer, для можно создать словарь в котором будут храниться Customer и в них будут добавляться их Preferences и PromoCodes. В нашем случае в словаре будет только один Customer и мы можем получить его по Id.

Последним параметром в запросе идёт параметр разделения splitOn, он указывает Dapper, какие столбцы использовать для разделения данных на несколько объектов.

Таким образом мы можем работать со сложными запросами и настраивать маппинг под свои нужды.

#### Non-Query Commands

Dapper позволяет выполнять команды, не связанные с запросом, которые не предназначены для возврата результирующих наборов, такие как инструкции INSERT, UPDATE и DELETE. Чтобы сделать это, необходимо использовать метод Execute extension интерфейса IDbConnection, который реализует Dapper.

Вставка:

``` csharp
var sql = "INSERT INTO Categories (CategoryName) values ('New Category')";
using (var connection = new SqlConnection(connectionString))
{
    var affectedRows =  connection.Execute(sql);

    Console.WriteLine($"Affected Rows: {affectedRows}");
}
```

Вставка Customer:

``` csharp
await using var sqlConnection = _connectionFactory.CreateSqlConnection();
int rowsAffected = await sqlConnection.ExecuteAsync(
    new CommandDefinition(
        """
        INSERT INTO public."Customers"
        ("Id", "FirstName", "LastName", "Email")
        VALUES(@Id, @FirstName, @LastName, @Email)
         """, 
        new { Id = entity.Id, FirstName = entity.FirstName, LastName = entity.LastName, Email = entity.Email }, 
        cancellationToken: cancellationToken));

return rowsAffected > 0
    ? entity 
    : throw new DatabaseException();
```

Обновление:

``` csharp
var sql = @"UPDATE Products SET TotalPrice = Quantity * UnitPrice WHERE CategoryID = 2";
using (var connection = new SqlConnection(connectionString))
{
    var affectedRows = connection.Execute(sql);

    Console.WriteLine($"Affected Rows: {affectedRows}");
}
```

Обновление Customer:

``` csharp
await using var sqlConnection = _connectionFactory.CreateSqlConnection();
int rowsAffected = await sqlConnection.ExecuteAsync(
    new CommandDefinition(
        """
        UPDATE public."Customers"
        SET "FirstName" = @FirstName, "LastName" = @LastName, "Email" = @Email
        WHERE "Id" = @Id
         """, 
        new { Id = entity.Id, FirstName = entity.FirstName, LastName = entity.LastName, Email = entity.Email }, 
        cancellationToken: cancellationToken));

return rowsAffected > 0
    ? entity 
    : throw new DatabaseException();
```

И удаление:

``` csharp
var sql = "DELETE FROM Categories WHERE CategoryName = 'New Category'";
using (var connection = new SqlConnection(connectionString))
{
    var affectedRows =  connection.Execute(sql);

    Console.WriteLine($"Affected Rows: {affectedRows}");
}
```

Удаление Customer:

``` csharp
await using var sqlConnection = _connectionFactory.CreateSqlConnection();
        return await sqlConnection.ExecuteAsync(
            new CommandDefinition(
            """DELETE FROM public."Customers" WHERE "Customers"."Id" = @id """, 
            new { id }, 
            cancellationToken: cancellationToken));
```

#### Buffered Query

По умолчанию Dapper, при выполнении SQL и буферизирует данные в памяти при возврате ответа. Весь результирующий набор будет извлечен из источника данных в память и сохранен в переменной. Это идеально в большинстве случаев, поскольку сводит к минимуму общие блокировки в БД и сокращает сетевое время БД.

Однако при выполнении больших запросов вам может потребоваться минимизировать объем памяти и загружать объекты только по мере необходимости. Для этого необходимо установить, buffered: false в метод запроса.

``` csharp
using (var connection = new SqlConnection(connectionString))
{
    var sql = "SELECT * FROM Customers";
    var customers = connection.Query<Customer>(sql, buffered: false);

    foreach(var customer in customers)
    {
        Console.WriteLine($"{customer.CustomerId} {customer.CompanyName}");
    }
}
```

#### Транзакции

Dapper предоставляет различные способы работы с транзакциями. Наиболее распространенным способом является использование метода beginTransaction, доступного в интерфейсе IDbConnection. Это вернет экземпляр IDbTransaction, который можно использовать с методами Execute и Query для добавления, удаления и изменения данных в таблицах вашей базы данных.

``` csharp
// Open the connection and start a transaction: 
using (var connection = new SqlConnection(connectionString))
{ 
    connection.Open();

    using (var tran = connection.BeginTransaction()) 
    { 
        // Execute your queries here

        tran.Commit(); //Or rollback 
    }
}
```

Также можно использовать класс TransactionScope, который предоставляет гораздо более простой способ работы с транзакциями:

``` csharp
// Open the connection and start a transaction: 
using (var scope = new TransactionScope()) 
{ 
    // Execute your queries here

    scope.Complete(); //Or it will automatically rollback 
}
```

### Заключение

На этом отчёт по данной целе закончен. Полезные ссылки:

- [Учебное приложение](https://gitlab.com/femto8032/2023q3/-/tree/main/Dapper?ref_type=heads)
- [Офф. документация](https://www.mongodb.com/docs/manual/replication/)
- [GitHub Dapper](https://github.com/DapperLib/Dapper)